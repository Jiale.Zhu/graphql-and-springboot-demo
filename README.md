# GraphQL and SpringBoot
Demo about GraphQL and SpringBoot integration.

# How to run
com.coxautodev.graphql.tools.example.GraphqlJavaToolsExampleApplication.main

# Endpoint
http://localhost:8080/playground

# Request sample
```
# Write your query or mutation here
{
  human(id: "1000") {
    id
    name
    friends {
      id
      name
      appearsIn
    }
    appearsIn
    homePlanet
  }
}
```

# GraphQL doc
- https://github.com/graphql-java-kickstart/graphql-spring-boot
- https://github.com/graphql-java-kickstart/graphql-java-tools/tree/master/example

